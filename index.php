<!DOCTYPE html>
<html>
<head>
	<title>In-100-грамм </title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<?php
	if(isset($_POST['tag'])){
		$tag = urlencode($_POST['tag']);		
	}else{
    $tags = array('studyinsweden', 'puppies','gothenburg','саратов','hisingen','communication', 'espressohouse');
		$tag = urlencode($tags[array_rand($tags, 1)]);
	}
?>
<body>
	<div class="form">
      <form class="form-signin" action="" method="post" >
           <input type="text" name="tag" placeholder="gimme a tag">   
           <button type="submit">Search</button>        
      </form>
      <?php if(isset($tag)){ 
          echo "<h4>showing tag #" . urldecode($tag) . " </h4>" ;
        }
      ?>
     </div>
<?php
	$token = "THIS IS TOKEN HERE" ; // GO TO https://www.instagram.com/developer/clients/manage/ TO GET ONE

  function fetchData($url){
    $ch = curl_init();
  	curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch); 
    return $result;
  }

  if(isset($_POST['new_url'])){
	  $result = fetchData($_POST['new_url']);  	
  }else{
	  $result = fetchData("https://api.instagram.com/v1/tags/" . $tag . "/media/recent/?client_id=" . $token ."&count=60" );
  }
  $result = json_decode($result);
  if(isset($result->pagination->next_url)){
	  $next_url = $result->pagination->next_url;
  }else{
  	$next_url = null;
  }

  echo "<table>";
  foreach ($result->data as $post) {
  	if(isset($post->caption->text)){
  		$img_label = $post->caption->text;
  	}else{
      $img_label = "";
    }
  	echo "<tr> <td class='image'>";
  	echo "<img src='" . $post->images->standard_resolution->url . "' ></td>";
  	echo "<td><p>" . $img_label . "</p> <p><b>Username:</b> <a href='http://www.instagram.com/".  $post->user->username ."' target='_blank'>". $post->user->username ."</a></p>";
    if(isset($post->location->name)){
      echo "<p><b>Location:</b>" . $post->location->name .  " </p>";
    }
  	echo "</td></tr>";  	
  }
  echo "</table>";
  if($next_url){ 
?>
  <form class="form" action="" method="post">
        <button name="new_url" value='<?php echo $next_url ?>' type="submit">Next</button>        
  </form>
<?php } ?>

</body>
</html>
